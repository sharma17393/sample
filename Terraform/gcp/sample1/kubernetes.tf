resource "google_container_cluster" "primary" {
  name               = "gke-tutorial-1"
  zone               = "us-west2-a"
  initial_node_count = 2
}
