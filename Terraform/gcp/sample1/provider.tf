provider "google" {
  credentials = "${file("account.json")}"
  project     = "terraform-gke-211705"
  region      = "us-west"
}
