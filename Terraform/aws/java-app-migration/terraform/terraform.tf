terraform {
    backend "s3" {
        encrypt = true
        bucket = "otsuka-tf-state"
        region = "us-east-1"
        key = "C:/Users/laptop/.aws/credentials"
        profile = "otsuka-dev"
        shared_credentials_file = "C:/Users/laptop/.aws/credentials"
    }
}