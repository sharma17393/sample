variable "aws_region" {
  description = "AWS region default value"
  default     = "us-east-1"
}

variable "cluster_name" {
  description = "RDS cluster name"
  default     = "ods-dev"
}
variable "security_group" {
  description = "RDS cluster security group"
  default     = "sg-3973c073"
}
variable "subnet_group_name" {
  description = "RDS cluster subnet group name"
  default     = "direct-rds-subgrp"
}
variable "instance_class" {
  description = "RDS instance type"
  default     = "db.r3.xlarge"
}
variable "database_name" {
  description = "SDM database name"
  default     = "data_portfolio_ii"
}
variable "db_instance" {
  description = "SDM db instance"
  default     = "ods-dev"
}
variable "ecr_repository" {
  description = "AWS ECR repository"
  default     = "opdc"
}
variable "username" {
  
}
variable "password" {
  
}
variable "ssh_user" {
  
}
