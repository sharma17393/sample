output "db_instance_name" {
  value = "${aws_db_instance.ods-dev.endpoint}"
}
output "ecr_repository" {
  value = "${aws_ecr_repository.opdc.repository_url}"
}

