resource "aws_db_instance" "ods-dev" {
  identifier = "${var.db_instance}"  
  allocated_storage    = 120
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.6"
  instance_class       = "${var.instance_class}"
  name                 = "${var.database_name}"
  username             = "${var.username}"
  password             = "${var.password}"
  parameter_group_name = "default.mysql5.6"
  db_subnet_group_name = "${var.subnet_group_name}"
  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]
  vpc_security_group_ids = ["${var.security_group}"]
  publicly_accessible = false
  skip_final_snapshot = true
  tags {
    "Account Admin" = "Mukesh Sharma"
    "Account Owner" = "Akshay Vashist"
    "Application Owner" = "Akshay Vashist"
    "Cost Center" = "2301"
    "Environment" = "Non-Compliant Environment"
    "Financial Owner" = "Akshay Vashist"
    "System" = "Not set"
  }
}
# provisioner "local-exec" {
#     command = "sleep 120; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -u '${var.ssh_user},' -k --ask-sudo-pass ansible/main.yml"
# }