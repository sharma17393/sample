provider "aws" {
  region = "${var.aws_region}"
  profile = "otsuka-dev"
  shared_credentials_file = "c:/Users/laptop/.aws/credentials"
}
