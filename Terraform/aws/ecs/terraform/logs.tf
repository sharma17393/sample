resource "aws_cloudwatch_log_group" "wf_log_group" {
    name = "/ecs/wf-app"
    retention_in_days = "30"
    tags{
        name = "wf-log-group"
    }
}
resource "aws_cloudwatch_log_stream" "wf_log_stream" {
  name = "wf-log-stream"
  log_group_name = "${aws_cloudwatch_log_group.wf_log_group.name}"
}
