variable "aws_region" {
  description = "The AWS region to spin up environment"
  default = "us-east-1"
}

variable "az_count" {
  description = "Number of AZs"
  default = 2
}
variable "app_image" {
  description = "Docker image for wildfly"
  default = "jboss/wildfly:9.0.1.Final"
}
variable "app_port" {
  description = "Default port to connect to app"
  default = 3000
}
variable "app_count" {
  description = "Number of instances"
  default = 2
}
variable "ecs_autoscale_role" {
  description = "Role arn for the ecsAutocaleRole"
  default     = "arn:aws:iam::753182529116:role/ecsAutoscaleRole"
}

variable "ecs_task_execution_role" {
  description = "Role arn for the ecsTaskExecutionRole"
  default     = "arn:aws:iam::753182529116:role/ecsTaskExecutionRole"
}

variable "health_check_path" {
  default = "/"
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "1024"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "2048"
}
