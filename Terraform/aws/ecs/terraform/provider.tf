provider "aws" {
  shared_credentials_file = "C:/Users/laptop/.aws/credentials"
  profile = "default"
  region = "${var.aws_region}"
}