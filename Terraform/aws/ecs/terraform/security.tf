resource "aws_security_group" "lb" {
  name = "wfly-load-balancer-sg"
  description = "controls access to the ALB"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
      protocol = "tcp"
      from_port = "8080"
      to_port = "8080"
      cidr_blocks = ["45.50.26.108/32"]
  }
  egress {
      protocol = "-1"
      from_port =   0
      to_port  = 0
      cidr_blocks = ["0.0.0.0/0"]
  }
}  
resource "aws_security_group" "ecs_tasks" {
  name = "wfly-ecs-tasks-sg"
  description = "allow inbound access from alb"
  vpc_id = "${aws_vpc.main.id}"
  ingress {
      protocol = "tcp"
      from_port = "${var.app_port}"
      to_port = "${var.app_port}"
      security_groups = ["${aws_security_group.lb.id}"]      
  }
  egress {
      protocol = "-1"
      from_port = 0
      to_port = 0
      cidr_blocks = ["0.0.0.0/0"]
  }
}

