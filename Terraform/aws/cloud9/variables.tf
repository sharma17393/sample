variable "policy" {
  default = "policy.json"
}

variable "user_name" {
  default = "cloud9"
}

variable "instance" {
  default = "t2.medium"
}
