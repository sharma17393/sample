resource "aws_iam_user" "cloud9" {
  name = "${var.user_name}"
}

resource "aws_iam_user_policy" "edXOptimizingPolicy" {
  name   = "edXOptimizingPolicy"
  policy = "${file(var.policy)}"
  user   = "${aws_iam_user.cloud9.name}"
}
